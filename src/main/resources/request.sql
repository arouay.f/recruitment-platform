CREATE DATABASE `recruitment-platform` ;

CREATE TABLE `recruitment-platform`.`civility` (
  `idcivility` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`idcivility`));

  CREATE TABLE `recruitment-platform`.`role` (
  `idrole` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`idcivility`));

    CREATE TABLE `recruitment-platform`.`governorate` (
  `idgovernorate` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`idcivility`));


INSERT INTO `recruitment-platform`.`civility`
(`idCivility`,
`name`)
VALUES
(1,
'Monsieur'),
(2,
'Madame'),
(3,
'Mademoiselle');


INSERT INTO `recruitment-platform`.`role`
(`idRole`,
`name`)
VALUES
(1,
'ROLE_UNIVERSITY'),
(2,
'ROLE_TEACHER');

INSERT INTO `recruitment-platform`.`governorate`
(`idGovernorate`,
`name`)
VALUES
(
    1,
'Ariana'
),
(
    2,
    'Béja'
),
(
    3,
    'Ben_Arous'
), 
(
    4,
    'Bizerte'
),
(
    5,
    'Gabès'
),
(
    6,
    'Gafsa'
),
(
    7,
    'Jendouba'
),
(
    8,
    'Kairouan'
),
(
    9,
    'Kasserine'
),
(
    10,
    'Kébili'
),
(
    11,
    'Le_Kef'
),
(
    12,
    'Mahdia'
),
(
    13,
    'La_Manouba'
),
(
    14,
    'Médenine'
),
(
    15,
    'Monastir'
),
(
    16,
    'Nabeul'
),
(
    17,
    'Sfax'
),
(
    18,
    'Sidi_Bouzid'
),
(
    19,
    'Siliana'
),
(
    20,
    'Sousse'
),
(
    21,
    'Tataouine'
),
(
    22,
    'Tozeur'
),
(
    23,
    'Tunis'
),
(
    24,
    'Zaghouan'
);
