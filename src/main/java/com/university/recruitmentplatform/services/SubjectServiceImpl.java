package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Subject;
import com.university.recruitmentplatform.repositories.SubjectRepository;
import org.springframework.stereotype.Service;

@Service
public class SubjectServiceImpl implements SubjectService {
    private SubjectRepository subjectRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    public boolean isSubjectExist(String name) {
        return subjectRepository.existsByName(name);
    }

    @Override
    public Subject addSubject(String name) {
        Subject subject = new Subject();
        subject.setName(name);
        return subjectRepository.save(subject);
    }

    @Override
    public Subject findSubjectByName(String name) {
        return subjectRepository.findByName(name);
    }
}
