package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.ERole;
import com.university.recruitmentplatform.models.Role;
import com.university.recruitmentplatform.repositories.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findRoleByName(ERole name) {
        return roleRepository.findByName(name);
    }
}
