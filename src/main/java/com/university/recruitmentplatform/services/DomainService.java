package com.university.recruitmentplatform.services;


import com.university.recruitmentplatform.models.Domain;

import java.util.List;

public interface DomainService {
    Domain findDomainByName(String name);
    Domain findDomainById(Long id);
    List<Domain> findAllDomains();
}
