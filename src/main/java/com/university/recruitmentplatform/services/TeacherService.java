package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.models.User;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

public interface TeacherService {
    Teacher addTeacher(Teacher teacher);
    Teacher findTeacherByUser(User user);
    Page<Teacher> findTeachersByDomain(Domain domain, int page);
    Set<Teacher> findBySubject(String name);
    Teacher findTeacherById(Long id);
}
