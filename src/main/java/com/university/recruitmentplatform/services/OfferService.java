package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Offer;
import com.university.recruitmentplatform.models.Subject;
import com.university.recruitmentplatform.models.University;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OfferService  {
    Offer save(Offer offer);

    List<Offer> findAllByApplicantsId(long applicantId);

    String deleteById(long id);

    Offer updateOffer(Offer offer);
    Page<Offer> getOffers(int page);

    Offer getOffer(Long id);
    Offer saveOffer(Offer offer);
    List<Offer> findOffersByOwner(University owner, int page);
    Page<Offer> findOffersByDomain(Domain domain, int page);
    Page<Offer> findOffersBySubject(Subject subject, int page);
}
