package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.University;
import com.university.recruitmentplatform.models.User;
import com.university.recruitmentplatform.repositories.UniversityRepository;
import org.springframework.stereotype.Service;

@Service
public class UniversityServiceImpl implements UniversityService {
    private UniversityRepository universityRepository;

    public UniversityServiceImpl(UniversityRepository universityRepository) {
        this.universityRepository = universityRepository;
    }

    @Override
    public University add(University university) {
        return universityRepository.save(university);
    }

    @Override
    public University findUniversityByUser(User user) {
        return universityRepository.findByUser(user);
    }
}
