package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.repositories.DomainRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DomainServiceImpl implements DomainService{
    private DomainRepository domainRepository;

    public DomainServiceImpl(DomainRepository domainRepository) {
        this.domainRepository = domainRepository;
    }

    @Override
    public Domain findDomainByName(String name) {
        return domainRepository.findByName(name);
    }

    @Override
    public Domain findDomainById(Long id) {
        return domainRepository.findById(id).get();
    }

    @Override
    public List<Domain> findAllDomains() {
        return domainRepository.findAll();
    }
}
