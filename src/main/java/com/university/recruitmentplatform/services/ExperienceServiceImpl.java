package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Experience;
import com.university.recruitmentplatform.repositories.ExperienceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExperienceServiceImpl implements ExperienceService {
    private ExperienceRepository experienceRepository;

    public ExperienceServiceImpl(ExperienceRepository experienceRepository) {
        this.experienceRepository = experienceRepository;
    }

    @Override
    public Experience addExperience(Experience experience) {
        return experienceRepository.save(experience);
    }

    @Override
    public List<Experience> findAllExperiences() {
        return experienceRepository.findAll();
    }
}
