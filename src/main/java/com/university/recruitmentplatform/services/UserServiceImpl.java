package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.dto.SignInRequest;
import com.university.recruitmentplatform.models.User;
import com.university.recruitmentplatform.repositories.UserRepository;
import com.university.recruitmentplatform.security.JwtUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private AuthenticationManager authenticationManager;
    private JwtUtil jwtUtil;
    private MyUserDetailsService myUserDetailsService;

    public UserServiceImpl(UserRepository userRepository, AuthenticationManager authenticationManager,
                           JwtUtil jwtUtil, MyUserDetailsService myUserDetailsService) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.myUserDetailsService = myUserDetailsService;
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User addUser(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public String signIn(SignInRequest request) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect Email Or Password", e);
        }
        return jwtUtil.generateToken((MyUserDetails)myUserDetailsService.loadUserByUsername(request.getEmail()));
    }

    @Override
    public void signIn(String email, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect Email Or Password", e);
        }
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).get();
    }
}
