package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Resume;

public interface ResumeService {
    Resume addResume(Resume resume);
}
