package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.*;
import com.university.recruitmentplatform.repositories.TeacherRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TeacherServiceImpl implements TeacherService {
    private TeacherRepository teacherRepository;
    private SubjectService subjectService;
    private ExperienceService experienceService;

    public TeacherServiceImpl(TeacherRepository teacherRepository, SubjectService subjectService, ExperienceService experienceService) {
        this.teacherRepository = teacherRepository;
        this.subjectService = subjectService;
        this.experienceService = experienceService;
    }

    @Override
    public Teacher addTeacher(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher findTeacherByUser(User user) {
        return teacherRepository.findByUser(user);
    }

    @Override
    public Page<Teacher> findTeachersByDomain(Domain domain, int page) {
        return teacherRepository.findAllByDomain(domain, PageRequest.of(page, 10));
    }

    @Override
    public  Set<Teacher> findBySubject(String name) {
        Set<Teacher> teachers = new HashSet<>();
        Subject subject = subjectService.findSubjectByName(name);
        for (Experience e : experienceService.findAllExperiences()) {
            if (e.getSubjects().contains(subject)) {
                Resume resume = e.getResume();
                teachers.add(teacherRepository.findByResume(resume));
            }
        }
        return teachers;
    }

    @Override
    public Teacher findTeacherById(Long id) {
        return teacherRepository.findById(id).get();
    }
}
