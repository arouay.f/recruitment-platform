package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.EGovernorate;
import com.university.recruitmentplatform.models.Governorate;

public interface GovernorateService {
    Governorate findGovernorateByName(EGovernorate name);
}
