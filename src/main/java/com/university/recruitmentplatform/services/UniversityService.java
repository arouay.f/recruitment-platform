package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.University;
import com.university.recruitmentplatform.models.User;

public interface UniversityService {
    University add(University university);
    University findUniversityByUser(User user);
}
