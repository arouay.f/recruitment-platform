package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Message;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.models.University;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MessageService {
    Message sendMessage(Message message);
    Page<Message> getTeacherMessages(Teacher teacher, int page);
    Page<Message> getUniversityMessages(University university, int page);
}
