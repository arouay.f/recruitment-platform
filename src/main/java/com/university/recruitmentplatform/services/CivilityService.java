package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Civility;
import com.university.recruitmentplatform.models.ECivility;

public interface CivilityService {
    Civility findCivilityByName(ECivility name);
}
