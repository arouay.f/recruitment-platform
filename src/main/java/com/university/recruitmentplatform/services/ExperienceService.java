package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Experience;

import java.util.List;

public interface ExperienceService {
    Experience addExperience(Experience experience);
    List<Experience> findAllExperiences();
}
