package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Resume;
import com.university.recruitmentplatform.repositories.ResumeRepository;
import org.springframework.stereotype.Service;

@Service
public class ResumeServiceImpl implements ResumeService {
    private ResumeRepository resumeRepository;

    public ResumeServiceImpl(ResumeRepository resumeRepository) {
        this.resumeRepository = resumeRepository;
    }

    @Override
    public Resume addResume(Resume resume) {
        return resumeRepository.save(resume);
    }
}
