package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Offer;
import com.university.recruitmentplatform.models.Subject;
import com.university.recruitmentplatform.models.University;
import com.university.recruitmentplatform.repositories.OfferRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    private OfferRepository offerRepository;

    public OfferServiceImpl(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public Offer save(Offer offer) {
        return this.offerRepository.save(offer);
    }

    @Override
    public List<Offer> findAllByApplicantsId(long applicantId) {
        return this.offerRepository.findAllByApplicantsId(applicantId);

    }

    @Override
    public String deleteById(long id) {
        this.offerRepository.deleteById(id);
        return "The offer with id = "+ id + "was successfully deleted";
    }

    @Override
    public Offer updateOffer(Offer offer) {
        Offer existingOffer = offerRepository.findById(offer.getId()).orElse(null);
        existingOffer.setAdded_date(offer.getAdded_date());
        existingOffer.setApplicants(offer.getApplicants());
        existingOffer.setDescription(offer.getDescription());
        existingOffer.setExpiration_date(offer.getExpiration_date());
        existingOffer.setOwner(offer.getOwner());
        existingOffer.setRequirements(offer.getRequirements());

        return offerRepository.save(existingOffer);
    }

    @Override
    public Page<Offer> getOffers(int page) {
        return offerRepository.findAll(PageRequest.of(page, 10));
    }

    @Override
    public Offer getOffer(Long id) {
        return offerRepository.findById(id).get();
    }

    @Override
    public Offer saveOffer(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    public List<Offer> findOffersByOwner(University owner, int page) {
        //return offerRepository.findByOwner(owner);
        return offerRepository.findAllByOwner(owner, PageRequest.of(page, 5));
    }

    @Override
    public Page<Offer> findOffersByDomain(Domain domain, int page) {
        return offerRepository.findByDomain(domain, PageRequest.of(page, 10));
    }

    @Override
    public Page<Offer> findOffersBySubject(Subject subject, int page) {
        return offerRepository.findBySubject(subject, PageRequest.of(page, 20));
    }

}
