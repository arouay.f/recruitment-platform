package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.EGovernorate;
import com.university.recruitmentplatform.models.Governorate;
import com.university.recruitmentplatform.repositories.GovernorateRepository;
import org.springframework.stereotype.Service;

@Service
public class GovernorateServiceImpl implements GovernorateService {
    private GovernorateRepository governorateRepository;

    public GovernorateServiceImpl(GovernorateRepository governorateRepository) {
        this.governorateRepository = governorateRepository;
    }

    @Override
    public Governorate findGovernorateByName(EGovernorate name) {
        return governorateRepository.findByName(name);
    }
}
