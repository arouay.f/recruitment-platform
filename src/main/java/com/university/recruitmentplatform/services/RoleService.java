package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.ERole;
import com.university.recruitmentplatform.models.Role;

public interface RoleService {
    Role findRoleByName(ERole name);
}
