package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Message;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.models.University;
import com.university.recruitmentplatform.repositories.MessageRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    private MessageRepository messageRepository;

    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Message sendMessage(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Page<Message> getTeacherMessages(Teacher teacher, int page) {
        return messageRepository.findAllByTeacher(teacher, PageRequest.of(page, 20));
    }

    @Override
    public Page<Message> getUniversityMessages(University university, int page) {
        return messageRepository.findAllByUniversity(university, PageRequest.of(page, 20));
    }
}
