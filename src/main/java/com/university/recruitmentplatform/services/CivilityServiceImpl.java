package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Civility;
import com.university.recruitmentplatform.models.ECivility;
import com.university.recruitmentplatform.repositories.CivilityRepository;
import org.springframework.stereotype.Service;

@Service
public class CivilityServiceImpl implements CivilityService {
    private CivilityRepository civilityRepository;

    public CivilityServiceImpl(CivilityRepository civilityRepository) {
        this.civilityRepository = civilityRepository;
    }

    @Override
    public Civility findCivilityByName(ECivility name) {
        return civilityRepository.findByName(name);
    }
}
