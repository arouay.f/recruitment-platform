package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.models.Subject;

public interface SubjectService {
    boolean isSubjectExist(String name);
    Subject addSubject(String name);
    Subject findSubjectByName(String name);
}
