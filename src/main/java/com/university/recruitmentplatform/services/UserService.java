package com.university.recruitmentplatform.services;

import com.university.recruitmentplatform.dto.SignInRequest;
import com.university.recruitmentplatform.models.User;

public interface UserService {
    boolean existsByEmail(String email);
    User addUser(User user);
    String signIn(SignInRequest request) throws Exception;
    void signIn(String email, String password) throws Exception;
    User findByEmail(String email);
}
