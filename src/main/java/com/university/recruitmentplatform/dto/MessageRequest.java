package com.university.recruitmentplatform.dto;

import lombok.Data;

@Data
public class MessageRequest {
    private String content;
}
