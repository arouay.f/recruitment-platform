package com.university.recruitmentplatform.dto;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Subject;
import lombok.Data;

@Data
public class OfferRequest {
    private String title;
    private String expirationDate;
    private String description;
    private String requirements;
    private Long domain;
    private String subject;
    private int period;
    private int numberHours;
    private float remuneration;
}
