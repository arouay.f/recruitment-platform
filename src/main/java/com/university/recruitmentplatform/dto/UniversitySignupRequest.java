package com.university.recruitmentplatform.dto;

import lombok.Data;

@Data
public class UniversitySignupRequest {
    private String email;
    private String password;
    private String name;
    private int phoneNumber;
    private String website;
    private String address;
    private String logo;
    private String description;
}
