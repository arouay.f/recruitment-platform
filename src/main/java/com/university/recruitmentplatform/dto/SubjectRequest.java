package com.university.recruitmentplatform.dto;

import lombok.Data;

@Data
public class SubjectRequest {
    private String name;
}
