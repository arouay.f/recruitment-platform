package com.university.recruitmentplatform.dto;

import lombok.Data;

import java.util.List;
@Data
public class ResumeRequest {
    private String hobbies;
    private String description;
    private List<ExperienceRequest> experiences;
}
