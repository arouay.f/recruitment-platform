package com.university.recruitmentplatform.dto;

import lombok.Data;

import java.util.List;
@Data
public class ExperienceRequest {
    private String name;
    private String date_begin;
    private String date_end;
    private List<SubjectRequest> subjects;
}
