package com.university.recruitmentplatform.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.ECivility;
import com.university.recruitmentplatform.models.EGovernorate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherSignupRequest {
    private String email;
    private String password;
    @JsonProperty("firstName")
    private String first_name;
    @JsonProperty("lastName")
    private String last_name;
    private ECivility civility;
    @JsonProperty("birthDate")
    private String birth_date;
    private EGovernorate governorate;
    private Long domain;
}
