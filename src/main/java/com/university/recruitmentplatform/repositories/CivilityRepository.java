package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.Civility;
import com.university.recruitmentplatform.models.ECivility;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CivilityRepository extends JpaRepository<Civility, Byte> {
    Civility findByName(ECivility name);
}
