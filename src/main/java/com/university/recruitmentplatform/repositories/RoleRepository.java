package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.ERole;
import com.university.recruitmentplatform.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Byte> {
    Role findByName(ERole name);
}
