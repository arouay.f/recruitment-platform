package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.Message;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.models.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    Page<Message> findAllByTeacher(Teacher teacher, Pageable pageable);
    Page<Message> findAllByUniversity(University university, Pageable pageable);
}
