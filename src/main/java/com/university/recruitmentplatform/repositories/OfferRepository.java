package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Offer;
import com.university.recruitmentplatform.models.Subject;
import com.university.recruitmentplatform.models.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {
    Page<Offer> findAll(Pageable pageable);
    List<Offer> findAllByApplicantsId(long applicantsIs);
    List<Offer> findByOwner(University university);
    List<Offer> findAllByOwner(University university, Pageable pageable);
    Page<Offer> findByDomain(Domain domain, Pageable pageable);
    Page<Offer> findBySubject(Subject subject, Pageable pageable);
}
