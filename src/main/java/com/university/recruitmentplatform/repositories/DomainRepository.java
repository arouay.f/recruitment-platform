package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainRepository extends JpaRepository<Domain,Long> {
    Domain findByName(String name);
}
