package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Resume;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    Teacher findByUser(User user);
    Page<Teacher> findAllByDomain(Domain domain, Pageable pageable);
    Teacher findByResume(Resume resume);
}
