package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.University;
import com.university.recruitmentplatform.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {
    University findByUser(User user);
}
