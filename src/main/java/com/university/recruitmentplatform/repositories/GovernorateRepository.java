package com.university.recruitmentplatform.repositories;

import com.university.recruitmentplatform.models.EGovernorate;
import com.university.recruitmentplatform.models.Governorate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GovernorateRepository extends JpaRepository<Governorate, Short> {
    Governorate findByName(EGovernorate name);
}
