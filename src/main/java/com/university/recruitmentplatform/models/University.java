package com.university.recruitmentplatform.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.university.recruitmentplatform.dto.UniversitySignupRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class University {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column( nullable = false, unique = true)
    private Long id;

    @Column(nullable = false, unique = false)
    private String name;

    @Column(nullable = false, unique = true)
    private String website;

    @Column( nullable = false, unique = true)
    private int phone_number;

    @Column( nullable = false, unique = false)
    private String address;

    @Column( nullable = false, unique = false)
    private String logo;

    @Column( nullable = false, unique = false)
    private String description;
    @OneToOne
    private User user;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    @JsonIgnore
    private List<Offer> offers = new ArrayList<>();

    @OneToMany(mappedBy = "teacher")
    private Set<Message> messages = new HashSet<>();

    public static University createUniversity(UniversitySignupRequest request) {
        University university = new University();
        university.setAddress(request.getAddress());
        university.setDescription(request.getDescription());
        university.setLogo(request.getLogo());
        university.setName(request.getName());
        university.setPhone_number(request.getPhoneNumber());
        university.setWebsite(request.getWebsite());
        return university;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        University that = (University) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
