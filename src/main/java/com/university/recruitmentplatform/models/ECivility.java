package com.university.recruitmentplatform.models;

public enum ECivility {
    Monsieur,
    Madame,
    Mademoiselle

}
