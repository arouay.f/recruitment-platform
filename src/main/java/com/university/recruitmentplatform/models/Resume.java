package com.university.recruitmentplatform.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Data
@Entity
@Table(name = "resume")
@NoArgsConstructor
@AllArgsConstructor
public class Resume {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resume_id")
    private Long id;

    @Column( nullable = false, unique = false)
    private String hobbies;
    @Column( nullable = false, unique = false)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "resume")
    private List<Experience> experiences = new ArrayList<>();
    @OneToOne(mappedBy = "resume")
    @JsonIgnore
    private Teacher teacher;

}
