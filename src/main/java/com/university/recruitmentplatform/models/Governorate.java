package com.university.recruitmentplatform.models;

import javax.persistence.*;

@Entity
@Table(name = "governorate")
public class Governorate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="idgovernorate" ,nullable = false)
    private Short id;
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private EGovernorate name;

    public Governorate(){
    }

    public Governorate(EGovernorate name) {
        this.name = name;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public EGovernorate getName() {
        return name;
    }

    public void setName(EGovernorate name) {
        this.name = name;
    }
}
