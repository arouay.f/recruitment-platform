package com.university.recruitmentplatform.models;

public enum EGovernorate {
    Ariana,
    Béja,
    Ben_Arous,
    Bizerte,
    Gabès,
    Gafsa,
    Jendouba,
    Kairouan,
    Kasserine,
    Kébili,
    Le_Kef,
    Mahdia,
    La_Manouba,
    Médenine,
    Monastir,
    Nabeul,
    Sfax,
    Sidi_Bouzid,
    Siliana,
    Sousse,
    Tataouine,
    Tozeur,
    Tunis,
    Zaghouan
}
