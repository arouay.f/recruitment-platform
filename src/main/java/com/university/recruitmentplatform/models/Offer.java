package com.university.recruitmentplatform.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

import java.time.Period;
import java.util.HashSet;
import java.util.Set;
@Entity
public class Offer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @ManyToOne
    private University owner;
    @OneToOne
    private Subject subject;
    @OneToOne
    private Domain domain;
    private Period period;
    private int numberHours;
    private float remuneration;
    @ManyToMany
    @JoinTable(name = "offer_teacher",
    joinColumns = @JoinColumn(name = "offer_id"),
    inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private Set<Teacher> applicants;
    private LocalDateTime added_date;
    private LocalDate expiration_date;
    private String description;
    private String requirements;

    public Offer(){
        applicants= new HashSet<>();
    }


    public Offer(University owner,  LocalDateTime added_date, LocalDate expiration_date, String description, String requirements,
                 Subject subject, Domain domain, Period period, int numberHours, float remuneration) {
        this.owner = owner;
        this.added_date = added_date;
        this.expiration_date = expiration_date;
        this.description = description;
        this.requirements = requirements;
        this.subject = subject;
        this.domain = domain;
        this.period = period;
        this.numberHours = numberHours;
        this.remuneration = remuneration;
    }

    public University getOwner() {
        return owner;
    }

    public void setOwner(University owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Teacher> getApplicants() {
        return applicants;
    }

    public void setApplicants(Set<Teacher> applicants) {
        this.applicants = applicants;
    }

    public LocalDateTime getAdded_date() {
        return added_date;
    }

    public void setAdded_date(LocalDateTime added_date) {
        this.added_date = added_date;
    }

    public LocalDate getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(LocalDate expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public int getNumberHours() {
        return numberHours;
    }

    public void setNumberHours(int numberHours) {
        this.numberHours = numberHours;
    }

    public float getRemuneration() {
        return remuneration;
    }

    public void setRemuneration(float remuneration) {
        this.remuneration = remuneration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
