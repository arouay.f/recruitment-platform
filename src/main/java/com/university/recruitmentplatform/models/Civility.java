package com.university.recruitmentplatform.models;

import javax.persistence.*;

@Entity
@Table(name = "civility")
public class Civility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="idcivility" ,nullable = false)
    private Byte id;
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ECivility name;

    public Civility(){
    }

    public Civility(ECivility name) {
        this.name = name;
    }

    public Byte getId() {
        return id;
    }

    public void setId(Byte id) {
        this.id = id;
    }

    public ECivility getName() {
        return name;
    }

    public void setName(ECivility name) {
        this.name = name;
    }
}
