package com.university.recruitmentplatform.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "experience")
public class Experience {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_experience")
    private Long id;

    @Column( nullable = false, unique = false)
    private String name;
    @Column( nullable = false, unique = false)
    private LocalDate date_begin;
    @Column( nullable = false, unique = false)
    private LocalDate date_end;

    @ManyToMany
    @JoinTable(name = "experience_subject",
            joinColumns = @JoinColumn(name = "experience_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id"))
    private List<Subject> subjects = new ArrayList<>();

    @ManyToOne
    @JsonIgnore
    private Resume resume;
}
