package com.university.recruitmentplatform.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name = "subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_subject")
    private Long id;
    @Column( nullable = false, unique = true)
    private String name;
    @ManyToMany(mappedBy = "subjects")
    @JsonIgnore
    private List<Experience> experiences;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        return id.equals(subject.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}
