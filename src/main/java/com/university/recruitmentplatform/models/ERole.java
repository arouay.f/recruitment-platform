package com.university.recruitmentplatform.models;

public enum ERole {
    ROLE_UNIVERSITY,
    ROLE_TEACHER
}
