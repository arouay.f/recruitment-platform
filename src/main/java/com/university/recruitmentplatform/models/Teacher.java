package com.university.recruitmentplatform.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(nullable = false, unique = false)
    private String first_name;

    @Column(nullable = false, unique = false)
    private String last_name;

    //@Column(nullable = false, unique = true)
    @Enumerated(EnumType.STRING)
    @OneToOne
    private Civility civility;

    @Column(nullable = false, unique = false)
    private LocalDate birth_date;

    //@Column(nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    @OneToOne
    private Governorate governorate;
    @ManyToMany(mappedBy = "applicants")
    @JsonIgnore
    private Set<Offer> offers = new HashSet<>();
    @OneToOne
    private User user;

    @OneToOne
    @JoinColumn(name = "domain_id", referencedColumnName = "id_domain")
    @JsonIgnore
    private Domain domain;

    @OneToMany(mappedBy = "university")
    @JsonIgnore
    private Set<Message> messages = new HashSet<>();



    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "resume_id", referencedColumnName = "resume_id")
    private Resume resume;

    public Teacher(String first_name, String last_name, LocalDate birth_date) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        return id.equals(teacher.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
