package com.university.recruitmentplatform.controllers;

import com.university.recruitmentplatform.dto.MessageRequest;
import com.university.recruitmentplatform.models.Message;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.models.University;
import com.university.recruitmentplatform.services.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/message")
public class MessageController {
    private UserService userService;
    private UniversityService universityService;
    private MessageService messageService;
    private TeacherService teacherService;

    public MessageController(UserService userService, UniversityService universityService, MessageService messageService,
                             TeacherService teacherService) {
        this.userService = userService;
        this.universityService = universityService;
        this.messageService = messageService;
        this.teacherService = teacherService;
    }
    @PostMapping("/university/{id}")
    public ResponseEntity<Message> sendMessage(@PathVariable Long id, @RequestBody MessageRequest messageRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        University university = universityService.findUniversityByUser(userService.findByEmail(userDetails.getEmail()));
        Message message = new Message();
        message.setContent(messageRequest.getContent());
        message.setSentDate(LocalDateTime.now());
        message.setUniversity(university);
        Teacher teacher = teacherService.findTeacherById(id);
        message.setTeacher(teacher);
        Message sentMessage = messageService.sendMessage(message);
        return ResponseEntity.ok(sentMessage);
    }
    @GetMapping("/teacher/mymessages")
    public ResponseEntity<Page<Message>> getTeacherMessages(@RequestParam(value = "page", defaultValue = "0") int page) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        Teacher teacher = teacherService.findTeacherByUser(userService.findByEmail(userDetails.getEmail()));
        Page<Message> myMessages = messageService.getTeacherMessages(teacher, page);
        //Map<String, Message> messages = new HashMap<>();
        //messages.put(myMessages.get(0).getUniversity(), myMessages.get(0).getTeacher());
        //myMessages.remove(0);
        /*for (Message m : myMessages) {
          messages.put(m.getUniversity().getName(), m);
        }*/
        return ResponseEntity.ok(myMessages);
    }

    @GetMapping("/university/mymessages")
    public ResponseEntity<Page<Message>> getUniversityMessages(@RequestParam(value = "page", defaultValue = "0") int page) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        University university = universityService.findUniversityByUser(userService.findByEmail(userDetails.getEmail()));
        Page<Message> myMessages = messageService.getUniversityMessages(university, page);
        int t = 0 ;
        //Map<String, Message> messages = new HashMap<>();
        //messages.put(myMessages.get(0).getUniversity(), myMessages.get(0).getTeacher());
        //myMessages.remove(0);
        /*for (Message m : myMessages) {
          messages.put(m.getUniversity().getName(), m);
        }*/
        return ResponseEntity.ok(myMessages);
    }
}
