package com.university.recruitmentplatform.controllers;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.services.DomainService;
import com.university.recruitmentplatform.services.TeacherService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/auth/teachers")
public class TeacherController {
    private TeacherService teacherService;
    private DomainService domainService;

    public TeacherController(TeacherService teacherService, DomainService domainService) {
        this.teacherService = teacherService;
        this.domainService = domainService;
    }

    @GetMapping("/domain/{name}")
    public ResponseEntity<Page<Teacher>> getTeachersByDomain(@RequestParam(value = "page", defaultValue = "0") int page,
                                                             @PathVariable String name) {
        Domain domain = domainService.findDomainByName(name);
        Page<Teacher> teachers = teacherService.findTeachersByDomain(domain, page);
        int t = 0;
        return ResponseEntity.ok(teachers);

    }
    @GetMapping("/subject")
    public ResponseEntity<Set<Teacher>> getTeachersBySubject(@RequestParam(value = "subject") String name) {
        return ResponseEntity.ok(teacherService.findBySubject(name));
    }
}
