package com.university.recruitmentplatform.controllers;

import com.university.recruitmentplatform.models.Domain;
import com.university.recruitmentplatform.services.DomainService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/auth/domains")
public class DomainController {
    private DomainService domainService;

    public DomainController(DomainService domainService) {
        this.domainService = domainService;
    }
    @GetMapping("/all")
    public ResponseEntity<List<Domain>> getDomains() {
        List<Domain> domains = domainService.findAllDomains();
        if (domains.isEmpty())
            return new ResponseEntity(domains, HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity(domains, HttpStatus.OK);
    }
}
