package com.university.recruitmentplatform.controllers;

import com.university.recruitmentplatform.dto.OfferRequest;
import com.university.recruitmentplatform.models.*;
import com.university.recruitmentplatform.services.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/offer")
public class OfferController {
    private OfferService offerService;
    private UniversityService universityService;
    private UserService userService;
    private SubjectService subjectService;
    private TeacherService teacherService;
    private DomainService domainService;

    public OfferController(UniversityService universityService, UserService userService, OfferService offerService,
                           SubjectService subjectService, TeacherService teacherService, DomainService domainService) {
        this.universityService = universityService;
        this.userService = userService;
        this.offerService = offerService;
        this.subjectService = subjectService;
        this.teacherService = teacherService;
        this.domainService = domainService;
    }


    @PostMapping("/university/create")
    public ResponseEntity<?> createOffer(@RequestBody OfferRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        University university = universityService.findUniversityByUser(userService.findByEmail(userDetails.getEmail()));
        Offer offer = new Offer();
        offer.setTitle(request.getTitle());
        offer.setOwner(university);
        offer.setAdded_date(LocalDateTime.now());
        offer.setExpiration_date(LocalDate.parse(request.getExpirationDate()));
        offer.setDescription(request.getDescription());
        offer.setRequirements(request.getRequirements());
        offer.setDomain(domainService.findDomainById(request.getDomain()));
        offer.setNumberHours(request.getNumberHours());
        offer.setRemuneration(request.getRemuneration());
        offer.setPeriod(Period.ofYears(request.getPeriod()));
        if (subjectService.isSubjectExist(request.getSubject())) {
            offer.setSubject(subjectService.findSubjectByName(request.getSubject()));
        } else {
            Subject subject = subjectService.addSubject(request.getSubject());
            offer.setSubject(subject);
        }
        Offer savedOffer = offerService.save(offer);
        return ResponseEntity.ok(savedOffer);
    }
    @GetMapping("/offers")
    public ResponseEntity<Page<Offer>> getOffers(@RequestParam(value = "page", defaultValue = "0") int page ) {
        return ResponseEntity.ok(offerService.getOffers(page));
    }
    @GetMapping("/get/{id}")
    public ResponseEntity<Offer> getOffer(@PathVariable Long id) {
        return ResponseEntity.ok(offerService.getOffer(id));
    }
    @GetMapping("/apply/{id}")
    public ResponseEntity<Offer> applyToOffer(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        Teacher teacher = teacherService.findTeacherByUser(userService.findByEmail(userDetails.getEmail()));
        Offer offer = offerService.getOffer(id);
        offer.getApplicants().add(teacher);
        Offer updatedOffer = offerService.saveOffer(offer);
        return ResponseEntity.ok(updatedOffer);
    }
    @GetMapping("/university/myoffers")
    public ResponseEntity<List<Offer>> universityOffers(@RequestParam(value = "page", defaultValue = "0") int page) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        University university = universityService.findUniversityByUser(userService.findByEmail(userDetails.getEmail()));
        List<Offer> offers = offerService.findOffersByOwner(university, page);
        return ResponseEntity.ok(offers);
    }

    @GetMapping("/teacher/myoffers")
    public ResponseEntity<Set<Offer>> teacherOffers() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        Teacher teacher = teacherService.findTeacherByUser(userService.findByEmail(userDetails.getEmail()));
        Set<Offer> offers = teacher.getOffers();
        return  ResponseEntity.ok(offers);
    }

    @GetMapping("/domain/{name}")
    public ResponseEntity<Page<Offer>> getOffersByDomain(@RequestParam(value = "page", defaultValue = "0") int page,
                                                             @PathVariable String name) {
        Domain domain = domainService.findDomainByName(name);
        Page<Offer> offers = offerService.findOffersByDomain(domain, page);
        int t = 0;
        return ResponseEntity.ok(offers);

    }
    @GetMapping("/offers/subject")
    public ResponseEntity<Page<Offer>> getOffersBySubject(@RequestParam(value = "page", defaultValue = "0") int page,
                                                          @RequestParam(value = "query") String name) {
        if (subjectService.findSubjectByName(name) != null) {
            Page<Offer> offers = offerService.findOffersBySubject(subjectService.findSubjectByName(name), page);
            return ResponseEntity.ok(offers);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
