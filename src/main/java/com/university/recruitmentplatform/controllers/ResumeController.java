package com.university.recruitmentplatform.controllers;

import com.university.recruitmentplatform.dto.ExperienceRequest;
import com.university.recruitmentplatform.dto.ResumeRequest;
import com.university.recruitmentplatform.dto.SubjectRequest;
import com.university.recruitmentplatform.models.Experience;
import com.university.recruitmentplatform.models.Resume;
import com.university.recruitmentplatform.models.Subject;
import com.university.recruitmentplatform.models.Teacher;
import com.university.recruitmentplatform.services.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;


@RestController
@RequestMapping("/teacher/resume")
public class ResumeController {
    private ResumeService resumeService;
    private UserService userService;
    private TeacherService teacherService;
    private SubjectService subjectService;
    private ExperienceService experienceService;

    public ResumeController(ResumeService resumeService, TeacherService teacherService, UserService userService, SubjectService subjectService,
                            ExperienceService experienceService) {
        this.resumeService = resumeService;
        this.teacherService = teacherService;
        this.userService = userService;
        this.subjectService = subjectService;
        this.experienceService = experienceService;
    }

    @PostMapping("/create")
    public ResponseEntity<Resume> createResume(@RequestBody ResumeRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        Teacher teacher = teacherService.findTeacherByUser(userService.findByEmail(userDetails.getEmail()));
        Resume resume = new Resume();
        resume.setHobbies(request.getHobbies());
        resume.setDescription(request.getDescription());
        resume.setTeacher(teacher);
        Resume savedResume = resumeService.addResume(resume);
        teacher.setResume(savedResume);
        teacherService.addTeacher(teacher);
        for (ExperienceRequest ex: request.getExperiences()
             ) {
            Experience experience = new Experience();
            experience.setName(ex.getName());
            experience.setDate_begin(LocalDate.parse(ex.getDate_begin()));
            experience.setDate_end(LocalDate.parse(ex.getDate_end()));
            for  (SubjectRequest subject : ex.getSubjects()) {
                if (subjectService.isSubjectExist(subject.getName())) {
                    experience.getSubjects().add(subjectService.findSubjectByName(subject.getName()));
                } else {
                    Subject savedSubject = subjectService.addSubject(subject.getName());
                    experience.getSubjects().add(savedSubject);
                }
            }
            experience.setResume(savedResume);
            Experience savedExperience = experienceService.addExperience(experience);
            resume.getExperiences().add(savedExperience);
        }
        Resume updatedResume = resumeService.addResume(resume);

        return ResponseEntity.ok(updatedResume);
    }

}
