package com.university.recruitmentplatform.controllers;

import com.university.recruitmentplatform.dto.MessageResponse;
import com.university.recruitmentplatform.dto.SignInRequest;
import com.university.recruitmentplatform.dto.TeacherSignupRequest;
import com.university.recruitmentplatform.dto.UniversitySignupRequest;
import com.university.recruitmentplatform.models.*;
import com.university.recruitmentplatform.security.JwtUtil;
import com.university.recruitmentplatform.services.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private TeacherService teacherService;
    private UniversityService universityService;
    private RoleService roleService;
    private CivilityService civilityService;
    private GovernorateService governorateService;
    private DomainService domainService;
    private JwtUtil jwtUtil;

    public AuthenticationController(PasswordEncoder passwordEncoder, UserService userService, TeacherService teacherService
    ,RoleService roleService, CivilityService civilityService, GovernorateService governorateService, JwtUtil jwtUtil, UniversityService universityService,
                                    DomainService domainService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.teacherService = teacherService;
        this.roleService = roleService;
        this.civilityService = civilityService;
        this.governorateService = governorateService;
        this.jwtUtil = jwtUtil;
        this.universityService = universityService;
        this.domainService = domainService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody SignInRequest request) throws Exception {
        String jwt = userService.signIn(request);
        return ResponseEntity.ok(new MessageResponse(jwt));
    }

    @PostMapping("/teacher/signup")
    public ResponseEntity<?> teacherSignup(@RequestBody TeacherSignupRequest request) throws Exception {
        if (userService.existsByEmail(request.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use"));
        }
        User user = new User(request.getEmail(), passwordEncoder.encode(request.getPassword()));
        user.getRoles().add(roleService.findRoleByName(ERole.ROLE_TEACHER));
        User savedUser = userService.addUser(user);
        Teacher teacher = new Teacher(request.getFirst_name(), request.getLast_name(),
                LocalDate.parse(request.getBirth_date()));
        teacher.setCivility(civilityService.findCivilityByName(request.getCivility()));
        teacher.setGovernorate(governorateService.findGovernorateByName(request.getGovernorate()));
        teacher.setDomain(domainService.findDomainById(request.getDomain()));
        teacher.setUser(savedUser);
        Teacher savedTeacher = teacherService.addTeacher(teacher);
        if (savedTeacher != null) {
            userService.signIn(request.getEmail(), request.getPassword());
        }
        return ResponseEntity.ok(new MessageResponse(jwtUtil.generateToken(MyUserDetails.build(savedUser))));

    }
    @PostMapping("/university/signup")
    public ResponseEntity<?> universitySignUp(@RequestBody UniversitySignupRequest request) {
        if (userService.existsByEmail(request.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use"));
        }
        User user = new User(request.getEmail(), passwordEncoder.encode(request.getPassword()));
        user.getRoles().add(roleService.findRoleByName(ERole.ROLE_UNIVERSITY));
        User savedUser = userService.addUser(user);
        University university = University.createUniversity(request);
        university.setUser(savedUser);
        universityService.add(university);
        return ResponseEntity.ok(new MessageResponse(jwtUtil.generateToken(MyUserDetails.build(savedUser))));
    }
    @GetMapping("/profile")
    public ResponseEntity<?> getProfile() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails)authentication.getPrincipal();
        SimpleGrantedAuthority teacherRole = new SimpleGrantedAuthority("ROLE_TEACHER");
        SimpleGrantedAuthority universityRole = new SimpleGrantedAuthority("ROLE_UNIVERSITY");
        if (userDetails.getAuthorities().contains(universityRole)) {
            University university = universityService.findUniversityByUser(userService.findByEmail(userDetails.getEmail()));
            return ResponseEntity.ok(university);
        } else if (userDetails.getAuthorities().contains(teacherRole)) {
            Teacher teacher = teacherService.findTeacherByUser(userService.findByEmail(userDetails.getEmail()));
            return ResponseEntity.ok(teacher);
        } else {
            return new ResponseEntity("no role", HttpStatus.NOT_FOUND);
        }
    }

}
